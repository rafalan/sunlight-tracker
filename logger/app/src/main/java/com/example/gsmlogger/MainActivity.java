package com.example.gsmlogger;
import androidx.annotation.RequiresApi;
import android.app.Activity;
import android.location.Location;
import android.location.LocationManager;
import android.telephony.TelephonyManager;
import android.telephony.*;
import android.content.Context;
import java.util.Collections;
import java.util.List;
import android.os.Build;
import android.Manifest;
import androidx.core.app.ActivityCompat;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.TimerTask;
import java.util.Timer;

public class MainActivity extends Activity {

    private boolean didGetPermission = false;
    private CallAPI api;
    private double latitude;
    private double longitude;
    private ArrayList weights;
    private TextView txtView;
    private ArrayList<Double> measurementWindow;
    double lastMeasurement;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        measurementWindow = new ArrayList();
        txtView = (TextView) findViewById(R.id.isOutdoorLabel);

        // weights calculated from training file--------------
        double meanWeight   = -2.856846905;
        double minWeight    = 0.057851952;
        double maxWeight	= -0.105782725;
        double rangeWeight  = -0.257564586;
        double stdWeight	= -0.037671894;
        double nCellsWeight = -2.997914623;
        weights = new ArrayList();
        weights.add(meanWeight);
        weights.add(minWeight);
        weights.add(maxWeight);
        weights.add(rangeWeight);
        weights.add(stdWeight);
        weights.add(nCellsWeight);
        //-----------------------------------------------

        // getting permissions
        int permissions_code = 42;
        String[] permissions = {Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.ACCESS_WIFI_STATE
        };

        ActivityCompat.requestPermissions(this, permissions, permissions_code);
        start();


    }

    public void start() {

        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                // ui update thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // record signal
                        getGSMData();
                        getGPSData();

                    }
                });
            }
        }, 0, 5000);
    }

    public void getGPSData() {
        Location location = getLastKnownLocation();

        if (location == null) {
            longitude = 0;
            latitude = 0;
        } else {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            //System.out.println(latitude + " " + longitude);
        }
    }

    private Location getLastKnownLocation() {
        LocationManager locationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }


    @RequiresApi(api = Build.VERSION_CODES.Q)
    public void getGSMData() {
        ArrayList<Double> signals = new ArrayList();
        long timeStamp = 0;
        TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        // get all cell towers
        List<CellInfo> cellInfoList = telephonyManager.getAllCellInfo();

        for (int i = 0; i < cellInfoList.size(); i++) {
            if (cellInfoList.get(i) instanceof CellInfoLte) {
                CellInfoLte cellInfoLte = (CellInfoLte) cellInfoList.get(i);
                Double rsrp = (double)(Integer) cellInfoLte.getCellSignalStrength().getRsrp();

                signals.add(rsrp);
                timeStamp = cellInfoList.get(i).getTimeStamp();
            }
        }

        IOD_Classifier(signals, timeStamp);
    }


    ///------ The AI -------------------------------------------------------//
    // pre-process data and extract features
    public ArrayList extractFeatures(ArrayList signal) {

        ArrayList features = new ArrayList();
        return features;
    }

    // calculate standard deviation
    public double getSTD(ArrayList signal) {

        double standardDeviation = 0;
        for (int i = 0; i < signal.size(); i++) {
            double num = (double) signal.get(i);
            standardDeviation += Math.pow(num - getMEAN(signal), 2);
        }
        return  Math.sqrt( standardDeviation/ signal.size() );
    }

    // calculate mean
    public double getMEAN(ArrayList signal) {

        double sum = 0.0;
        for (int i = 0; i < signal.size(); i++) {
            double tmp = (double)signal.get(i);
            sum +=  tmp;
        }
        return sum / signal.size();

    }

    // dot product
    public double dotProduct(ArrayList a, ArrayList b) {
        double  sum = 0;
        for (int i = 0; i < a.size(); i++) {
            sum += (double)a.get(i) * (double)b.get(i);
        }
        return sum;
    }

    // sigmoid function
    public double sigmoid(double z) {
        return (1/( 1 + Math.pow(Math.E,(-1*z))));
    }

    // standard scaling the data using the values calculated from the training program
    public ArrayList standardScaleTransform(ArrayList data) {


        // mean of training data columns
        double u0 =-94.76100605;
        double u1= -101.853641;
        double u2=  -86.25739005;
        double u3=   15.5962509;
        double u4= 7.375772315;
        double u5= 4.062004326;

        // std of training data columns
        double s0 = 11.45161394;
        double s1 = 12.30865933;
        double s2 = 11.61811517;
        double s3 = 7.631100474;
        double s4 = 3.164909095;
        double s5 = 2.357736892;

        // z = (x - u) / s     --> (datapoint - mean) / standard_dev

        ArrayList scaled = new ArrayList();
        double z0 = ((double)data.get(0)-u0)/s0;
        double z1 = ((double)data.get(1)-u1)/s1;
        double z2 = ((double)data.get(2)-u2)/s2;
        double z3 = ((double)data.get(3)-u3)/s3;
        double z4 = ((double)data.get(4)-u4)/s4;
        double z5 = ((double)data.get(5)-u5)/s5;
        scaled.add(z0);
        scaled.add(z1);
        scaled.add(z2);
        scaled.add(z3);
        scaled.add(z4);
        scaled.add(z5);

        return scaled;
    }

    // prediction -> use the pre-processed features here
    public double predict(ArrayList features) {
        double z = dotProduct(features, weights);
        return sigmoid(z);
    }

    public void IOD_Classifier(ArrayList<Double> signal, long timeStamp) {


        // converting raw data to features
        double f1 = getMEAN(signal);
        double f2 = Collections.min(signal);
        double f3 = Collections.max(signal);
        double f4 = f3-f2;
        double f5 = getSTD(signal);
        double f6 = signal.size();

        ArrayList features = new ArrayList();
        features.add(f1);
        features.add(f2);
        features.add(f3);
        features.add(f4);
        features.add(f5);
        features.add(f6);

        // scaling features
        ArrayList<Double> scaledFeatures = standardScaleTransform(features);


        double predictionProbability = predict(scaledFeatures);


        /*
        lastMeasurement = predictionProbability;
        if (measurementWindow.size() == 0) {
            measurementWindow.add(predictionProbability);
        }


        else if ( !(Math.abs(lastMeasurement - measurementWindow.get(measurementWindow.size()-1)) <= 0.00001)) {
            measurementWindow.add(lastMeasurement);
            if (measurementWindow.size() > 3) {
                measurementWindow.remove(0);
            }
        }


        double sum = 0;
        if (measurementWindow.size() > 0) {

            for (int i = 0; i < measurementWindow.size(); i++) {
                sum += measurementWindow.get(i);
            }
            sum = sum / measurementWindow.size();
        }


*/
        boolean isOutdoor = (predictionProbability<0.5) ? true:false;


        System.out.println(isOutdoor);
        //--------------------
        String outdoor = "";
        if (isOutdoor) { outdoor = "You are outdoor   " + " probability = " + predictionProbability + " -  " + signal.toString(); }
        else { outdoor = "You are indoor    " + " probability = " + predictionProbability + " -  " + signal.toString();}
        txtView.setText(outdoor);


        System.out.println(measurementWindow.size());

        //if (measurementWindow.size() > 0) {
            post(isOutdoor, timeStamp);
        //}

    }

    //---------------------------------------------------------

    public void post(boolean isOutdoor, long timestamp) {
        String status;
        if (isOutdoor) {
            status = "out";
        } else {
            status = "in";
        }

        long unixtime = System.currentTimeMillis()/1000L;


        JSONObject manJson = new JSONObject();

        try {
            manJson.put("status", status);
            manJson.put("timestamp", unixtime);
            manJson.put("lat", latitude);
            manJson.put("lon", longitude);




        } catch (JSONException e) {
            e.printStackTrace();
        }


        String data = manJson.toString();
        System.out.println(data);
        api = new CallAPI();
        api.execute("http://sunlight.rocks/api/tracker/123", data);
    }
}
