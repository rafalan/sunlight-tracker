import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Colors from '../constants/Colors';
import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import StatisticsScreen from '../screens/StatisticsScreen';
import UserScreen from '../screens/UserScreen';

const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={'home'}
    />
  ),
};

HomeStack.path = '';

const StatisticsStack = createStackNavigator(
  {
    Statistics: StatisticsScreen,
  },
  config
);

StatisticsStack.navigationOptions = {
  tabBarLabel: 'Statistics',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={'activity'} />
  ),
};

StatisticsStack.path = '';

const UserStack = createStackNavigator(
  {
    User: UserScreen,
  },
  config
);

UserStack.navigationOptions = {
  tabBarLabel: 'User',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon focused={focused} name={'user'} />
  ),
};

UserStack.path = '';

const tabNavigator = createBottomTabNavigator(
  {
    HomeStack,
    StatisticsStack,
    UserStack,
  },
  {
    tabBarOptions: {
      activeTintColor: Colors.tabIconSelected,
      inactiveTintColor: Colors.tabIconDefault,
      style: {
        backgroundColor: Colors.backgroundColorTabBar,
        height: 60
      },
      // showLabel: false
    }
  });

tabNavigator.path = '';

export default tabNavigator;
