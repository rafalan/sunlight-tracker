import React from 'react';
import { SafeAreaView, StyleSheet, ScrollView, Text } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { View } from 'react-native-animatable';
import Colors from '../constants/Colors';
import ScrollChips from '../components/ScrollChips';
import Charts from '../components/Charts';
import constants, { getDaysInMonth } from '../constants/Layout';

const { width, height } = constants.window;

export default class StatisticsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
      title: null,
      hideHeader: true,
      headerTransparent: true,
      headerStyle: {
        borderBottomWidth: 0
      }
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      chips: [
        {
          id: "0001",
          title: "October",
          selected: true,
          filterString: 'October',
          filterType: 9
        },
        {
          id: "0002",
          title: "November",
          selected: false,
          filterString: 'November',
          filterType: 10
        },
        {
          id: "0003",
          title: "December",
          selected: false,
          filterString: 'December',
          filterType: 11
        }
      ],
      selectedChipType: undefined,
      chartsData: [
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100,
        Math.random() * 100
      ],
      labels: getDaysInMonth(10, 2019).map((item, index) => index + 1)
    }
    this.filterString = '';
  }

  render() {
    const { chips, labels, chartsData } = this.state
    return (
      <LinearGradient
        start={{ x: 0, y: 0.75 }}
        end={{ x: 1, y: 0.25 }}
        colors={Colors.backgroundColorGradient}
        style={[styles.container, { minWidth: width }]}
      >
        <SafeAreaView>
          <ScrollView
          // style={{ flex: 1 }}
          // contentContainerStyle={{ paddingTop: 30 }}
          >
            <View style={{ paddingHorizontal: 25, paddingTop: 30 }}>
              <Text style={{ fontSize: 36, fontWeight: '800', color: Colors.textColor }}>Monthly statistics</Text>
            </View>
            <ScrollChips data={chips} onPress={this.changeFilter} />
            <View style={{ alignItems: 'center' }}>
              <Charts
                labels={labels}
                chartsData={chartsData}
              />
              <Charts
                labels={labels}
                chartsData={chartsData}
              />
              <Charts
                labels={labels}
                chartsData={chartsData}
              />
              <Charts
                labels={labels}
                chartsData={chartsData}
              />
            </View>

          </ScrollView>
        </SafeAreaView>
      </LinearGradient>
    );
  }

  changeFilter = (id) => {
    this.setState(prevState => ({
      ...prevState,
      chips: prevState.chips.map(
        item => item.id === id ? ({ ...item, selected: true }) : ({ ...item, selected: false })
      ),
      selectedChipType: prevState.chips.find(item => item.id === id).filterType,
      labels: new Array(getDaysInMonth(prevState.chips.find(item => item.id === id).filterType))
    }));
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
