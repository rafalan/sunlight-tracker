import React from 'react';
import { ActivityIndicator, ScrollView, View, StyleSheet, Text } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Avatar } from 'react-native-paper';
import Constants from 'expo-constants';
import Colors from '../constants/Colors';
import layout from '../constants/Layout';
import axios from 'axios';
const { width } = layout.window;

export default class UserScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAppReady: false,
      clientData: {}
    }
  }

  render() {
    const { isAppReady, clientData } = this.state;
    const clientScreenItems = [
      { id: '0001', title: 'Name:', value: `${clientData.first_name ? clientData.first_name : "Junction"} ${clientData.last_name ? clientData.last_name : "user"}` },
      { id: '0002', title: 'Phone:', value: `${clientData.phone ? clientData.phone : '+358 (0) 9 172 881'} ` },
      { id: '0003', title: 'Birthday:', value: `${clientData.dob ? clientData.dob : '2019-10-11'} ` },
      { id: '0004', title: 'E-mail:', value: `${clientData.email ? clientData.email : 'test@test.com'} ` },
      { id: '0005', title: 'Location:', value: `${clientData.location ? clientData.location : 'Helsinki'} ` }
    ]
    if (!isAppReady) {
      return (
        <LinearGradient
          start={{ x: 0, y: 0.75 }}
          end={{ x: 1, y: 0.25 }}
          colors={Colors.backgroundColorGradient}
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
          onLayout={this._getDataFromServer}
        >
          <ActivityIndicator size="large" color={Colors.tintColor} />
        </LinearGradient>
      );
    }
    return (
      <LinearGradient
        start={{ x: 0, y: 0.75 }}
        end={{ x: 1, y: 0.25 }}
        colors={Colors.backgroundColorGradient}
        style={[styles.container, { minWidth: width }]}
      >
        <ScrollView
          style={styles.contentContainer}
          contentContainerStyle={styles.contentContainerStyle}
        >
          <View style={{ paddingHorizontal: 25 }}>
            <Text style={{ fontSize: 36, fontWeight: '800', color: Colors.textColor }}>User</Text>
          </View>
          <View style={{ alignItems: 'center' }}>
            <Avatar.Image size={100} source={require('../assets/images/default_avatar.jpg')} />
          </View >
          <View style={styles.speakerContainer}>
            {clientScreenItems.map(
              item =>
                <SpeakerScreenItem
                  onPressToText={item.title === 'Телефон:' ? onPressToPhone = () => { } : undefined}
                  key={item.id}
                  title={item.title}
                  value={item.value}
                  style={styles.speakerItem}
                />
            )}
          </View>
        </ScrollView>
      </LinearGradient>
    )
  }

  _getDataFromServer = () => {
    api()
      .then(
        ({ data }) => {
          this.setState({ clientData: data, isAppReady: true })
        }
      )
      .catch(
        err => console.log(err, err.response)
      )
  }
}

const api = () => {
  return axios.get('http://sunlight.rocks/api/user-details/123')
}

const SpeakerScreenItem = ({ title = "Заголовок", value = "Значение", style, onPressToText = (value) => console.log('onPressToText', value) }) => {
  return (
    <View style={{ ...layout.border, ...style }}>
      <Text style={styles.title}>{title}</Text>
      <Text style={title === 'Телефон:' ? styles.phone : styles.value} onPress={() => onPressToText(value.trim())}>{value}</Text>
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  contentContainer: {

  },
  contentContainerStyle: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingTop: Constants.statusBarHeight + 30
  },
  speakerContainer: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 15
  },
  speakerItem: {
    flexDirection: 'column',
    paddingVertical: 5,
    paddingHorizontal: 20
  },
  title: {
    paddingVertical: 10,
    fontSize: 20,
    color: Colors.textColor
  },
  value: {
    fontSize: 22,
    color: Colors.textColor,
    fontWeight: '700'
  },
  phone: {
    fontSize: 22,
    color: Colors.tintColor,
    fontWeight: '700'
  }
});

UserScreen.navigationOptions = ({ navigation }) => {
  return {
    title: null,
    headerTransparent: true,
    headerStyle: {
      opacity: (!navigation.state.params ? 0 : navigation.state.params.opacityValue),
      // height: (!navigation.state.params ? 0 : navigation.state.params.animatedValue)
    },
    headerTintColor: Colors.tintColor,

  }
};
