import React from 'react';
import {
  StyleSheet,
  Text,
  ActivityIndicator,
  View,
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import axios from 'axios';
import Constants from 'expo-constants';
import Colors from '../constants/Colors';
import constants from '../constants/Layout';
import ColorBar from '../components/ColorBar';
import LeftColunm from '../components/LeftColunm';
import Button from '../components/Button';
import Modal from '../components/Modal';

const { width } = constants.window

export default class HomeScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
      title: null,
      hideHeader: true,
      headerTransparent: true,
      headerStyle: {
        borderBottomWidth: 0
      }
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      percent: 0,
      image: undefined,
      isAppReady: false,
      isFocused: false,
      status: "",
      modalIsVisible: false
    }
  }

  show = () => {
    this.interval = setInterval(() => {
      this.setState(prevState => ({ ...prevState, percent: prevState.percent >= 1 ? 1 : prevState.percent + 0.01 }));
    }, 100);
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {
      console.log('focusListener');
      this.setState({ isFocused: true });
      // this.show()
    });
    this.focusListener = navigation.addListener("willBlur", () => {
      if (this.interval) {
        clearInterval(this.interval);
      }
    })
  }

  componentWillUnmount() {
    this.setState({ isFocused: false })
    if (this.interval) {
      clearInterval(this.interval);
    }
    this.focusListener.remove();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.percent !== this.state.percent && this.state.percent === 1) {
      if (this.interval) {
        clearInterval(this.interval);
      }
    }
  }

  render() {
    const { percent, isAppReady, isFocused, status, modalIsVisible } = this.state;

    if (!isAppReady) {
      return (
        <LinearGradient
          start={{ x: 0, y: 0.75 }}
          end={{ x: 1, y: 0.25 }}
          colors={Colors.backgroundColorGradient}
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
          onLayout={this._getDataFromServer.bind(this)}
        >
          <ActivityIndicator size="large" color={Colors.tintColor} />
        </LinearGradient>
      );
    }
    return (
      <LinearGradient
        start={{ x: 0, y: 0.75 }}
        end={{ x: 1, y: 0.25 }}
        colors={Colors.backgroundColorGradient}
        style={[styles.container, { minWidth: width, flexDirection: 'row' }]}
      >
        <View
          style={{ flex: 0.8 }}
        >
          <LeftColunm
            setStatus={this.setStatus.bind(this)}
            percent={percent}
            startAnimation={this.startAnimation}
            status={status}
            isFocused={isFocused}
          />
          <View style={{ alignItems: 'flex-start', paddingHorizontal: 20, paddingBottom: 20 }}>
            <Button onPress={this.onClickToButton} />
          </View>

        </View>
        <View
          style={{ flex: 0.2, paddingTop: Constants.statusBarHeight + 35, padding: 20 }}
        >
          <View
            style={{ justifyContent: 'center', alignItems: 'center', paddingVertical: 10 }}>
            <Text
              style={{ color: Colors.textColor, fontSize: 22, fontWeight: '800' }}
            >
              {Math.round(percent * 100)}%
            </Text>
          </View>
          <ColorBar percent={percent} isFocused={isFocused} />
        </View>
        <Modal
          isVisible={modalIsVisible}
          onCancel={this.onClickToButton}
        />
      </LinearGradient>
    );
  }

  onClickToButton = () => {
    const { modalIsVisible } = this.state;
    this.setState({ modalIsVisible: !modalIsVisible })
  }

  _getDataFromServer = () => {
    api()
      .then(
        ({ data }) => {
          console.log('_getDataFromServer', data);
          this.setState({ percent: data.percent / 100, isAppReady: true });
        }
      )
      .catch(
        err => console.log(err, err.response)
      )
  }

  setStatus = (status) => {
    this.setState({ status });
  }
}

const api = () => {
  return axios.get('http://sunlight.rocks/api/user-amount/123')
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
});
