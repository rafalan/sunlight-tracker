import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import axios from 'axios';
import * as Font from 'expo-font';
import React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import * as TaskManager from 'expo-task-manager';

import AppNavigator from './navigation/AppNavigator';

const LOCATION_TASK_NAME = 'background-location-task';

export default class App extends React.Component {
  state = {
    isLoadingComplete: false
  }

  componentDidMount() {
    this._getLocationAsync()
  }

  render() {
    const { isLoadingComplete } = this.state;
    const { skipLoadingScreen } = this.props;
    if (!isLoadingComplete && !skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this.loadResourcesAsync}
          onError={this.handleLoadingError}
          onFinish={() => this.setLoadingComplete(true)}
        />
      );
    } else {
      return (
        <View style={styles.container}>
          {Platform.OS === 'ios' && <StatusBar barStyle='light-content' />}
          <AppNavigator />
        </View>
      );
    }
  }

  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    console.log('_getLocationAsync', status);

    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    await Location.startLocationUpdatesAsync(LOCATION_TASK_NAME, {
      accuracy: Location.Accuracy.Balanced,
    });
    this.setState({ location });
    this.sendData(location)
  };

  sendData = (location) => {
    api(location)
      .then(
        ({ data }) => {
          console.log('====================================')
          console.log('data', data)
          console.log('====================================')
        }
      )
      .catch(
        err => console.log(err, err.response)
      )
  }

  loadResourcesAsync = async () => {
    await Promise.all([
      Asset.loadAsync([
        require('./assets/images/robot-dev.png'),
        require('./assets/images/robot-prod.png'),
      ]),
      Font.loadAsync({
        ...Ionicons.font,
        'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
      }),
    ]);
  }

  setLoadingComplete = (isLoadingComplete) => {
    this.setState({ isLoadingComplete })
  }

  handleLoadingError = (error) => {
    console.warn(error);
  }
}

TaskManager.defineTask(LOCATION_TASK_NAME, ({ data, error }) => {
  if (error) {
    console.log('====================================')
    console.log('TaskManager error', error)
    console.log('====================================')
    return;
  }
  if (data) {
    const { locations } = data;


    api(locations[0])
      .then(
        ({ data }) => {
          console.log('====================================')
          console.log('data', data)
          console.log('====================================')
        }
      )
      .catch(
        err => console.log(err, err.response)

      )
  }
});

const api = (locations) => {
  const dataToSend = {
    "timestamp": Number(String(locations.timestamp).substring(0, 10)),
    "status": locations.coords.accuracy < 25 ? "out" : "in",
    "lat": locations.coords.latitude,
    "lon": locations.coords.longitude
  }
  return axios.post('http://sunlight.rocks/api/tracker/123', dataToSend)
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
