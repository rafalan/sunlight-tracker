import React, { Component } from "react";
import { View, Text } from 'react-native';
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
} from "react-native-chart-kit";

import Colors from '../constants/Colors';
import constants from '../constants/Layout';
const { width, height } = constants.window;

export default class Charts extends Component {
    render() {
        return (
            <View>
                {/* <Text>Bezier Line Chart</Text> */}
                <LineChart
                    data={{
                        labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
                        datasets: [
                            {
                                data: [
                                    Math.random() * 100,
                                    Math.random() * 100,
                                    Math.random() * 100,
                                    Math.random() * 100,
                                    Math.random() * 100,
                                    Math.random() * 100,
                                    Math.random() * 100,
                                ]
                            }
                        ]
                    }}
                    width={width-40} // from react-native
                    height={220}
                    yAxisLabel={""}
                    yAxisSuffix={""}
                    xAxisLabel={""}
                    xAxisSuffix={""}
                    chartConfig={{
                        backgroundColor: Colors.backgroundColorCharts,
                        backgroundGradientFrom: "#4a88f7",
                        backgroundGradientTo: "#7bd0fa",
                        decimalPlaces: 2, // optional, defaults to 2dp
                        color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        style: {
                            borderRadius: 16
                        },
                        propsForDots: {
                            r: "6",
                            strokeWidth: "2",
                            stroke: "#4784f7"
                        }
                    }}
                    bezier
                    style={{
                        marginVertical: 8,
                        borderRadius: 16
                    }}
                />
            </View>
        );
    }
}