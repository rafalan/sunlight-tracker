import React from 'react';
import { StyleSheet, View, Text, TouchableHighlight } from "react-native";
import ReactNativeModal from "react-native-modal";
import Colors from '../constants/Colors';
import { isIphoneX } from '../constants/Layout';

export default class Modal extends React.Component {
    static defaultProps = {
        cancelTextIOS: "Cancel",
        confirmTextIOS: "Ok",
        date: new Date(),
        dismissOnBackdropPressIOS: true,
        hideTitleContainerIOS: false,
        isDarkModeEnabled: false,
        isVisible: false,
        mode: "date",
        neverDisableConfirmIOS: false,
        onHideAfterConfirm: () => { },
        onDateChange: () => { },
        reactNativeModalPropsIOS: {},
        titleIOS: "Recommendations",
        onCancel: () => { }
    };

    constructor(props) {
        super(props);
        this.state = {
            userIsInteractingWithPicker: false,
        }
    }

    render() {
        const {
            handleModalHide,
            handleModalShow,
            cancelTextIOS,
            cancelTextStyle,
            confirmTextIOS,
            confirmTextStyle,
            contentContainerStyleIOS,
            cancelButtonContainerStyleIOS,
            customCancelButtonIOS,
            customConfirmButtonIOS,
            customConfirmButtonWhileInteractingIOS,
            customDatePickerIOS,
            customTitleContainerIOS,
            datePickerContainerStyleIOS,
            dismissOnBackdropPressIOS,
            hideTitleContainerIOS,
            isDarkModeEnabled,
            isVisible,
            minuteInterval,
            mode,
            neverDisableConfirmIOS,
            pickerRefCb,
            reactNativeModalPropsIOS,
            titleIOS,
            titleStyle,
            ...otherProps
        } = this.props;

        const reactNativeModalProps = {
            onBackdropPress: dismissOnBackdropPressIOS
                ? this.handleCancel
                : () => null,
            ...reactNativeModalPropsIOS
        };

        const titleContainer = (
            <View style={styles.titleContainer}>
                <Text style={[styles.title, titleStyle]}>{titleIOS}</Text>
            </View>
        );
        let confirmButton;
        if (customConfirmButtonIOS) {
            if (
                customConfirmButtonWhileInteractingIOS &&
                this.state.userIsInteractingWithPicker
            ) {
                confirmButton = customConfirmButtonWhileInteractingIOS;
            } else {
                confirmButton = customConfirmButtonIOS;
            }
        } else {
            confirmButton = (
                <Text style={[styles.confirmText, confirmTextStyle]}>
                    {confirmTextIOS}
                </Text>
            );
        }
        const cancelButton = (
            <Text style={[styles.cancelText, cancelTextStyle]}>{cancelTextIOS}</Text>
        );

        const backgroundColor = isDarkModeEnabled ? BACKGROUND_COLOR_DARK : BACKGROUND_COLOR_LIGHT;

        return (
            <ReactNativeModal
                isVisible={isVisible}
                style={[styles.contentContainer, contentContainerStyleIOS]}
                onModalHide={handleModalHide}
                onModalShow={handleModalShow}
                backdropOpacity={0.4}
                {...reactNativeModalProps}
            >
                <View style={[styles.datepickerContainer, { backgroundColor }, datePickerContainerStyleIOS]}>
                    {!hideTitleContainerIOS &&
                        (customTitleContainerIOS || titleContainer)}
                    <View
                        onStartShouldSetResponderCapture={
                            neverDisableConfirmIOS !== true ? this.handleUserTouchInit : null
                        }
                    >
                    </View>
                    <TouchableHighlight
                        style={styles.confirmButton}
                        underlayColor={HIGHLIGHT_COLOR}
                        onPress={this.handleConfirm}
                        disabled={
                            !neverDisableConfirmIOS && this.state.userIsInteractingWithPicker
                        }
                    >
                        {confirmButton}
                    </TouchableHighlight>
                </View>

                {/* <TouchableHighlight
                    style={[styles.cancelButton, { backgroundColor }, cancelButtonContainerStyleIOS]}
                    underlayColor={HIGHLIGHT_COLOR}
                    onPress={this.handleCancel}
                >
                    {customCancelButtonIOS || cancelButton}
                </TouchableHighlight> */}
            </ReactNativeModal>
        );
    }

    handleConfirm = () => {
        this.props.onCancel();
    };

    handleDateChange = () => {
        this.setState({
            userIsInteractingWithPicker: false
        });
    };

    handleUserTouchInit = () => {
        if (!this.props.customDatePickerIOS) {
            this.setState({
                userIsInteractingWithPicker: true
            });
        }
        return false;
    };

    handleCancel = () => {
        this.props.onCancel();
    };
}

const BORDER_RADIUS = 13;
const BORDER_COLOR = "#d5d5d5";
const TITLE_FONT_SIZE = 13;
const TITLE_COLOR = "#8f8f8f";
const BUTTON_FONT_WEIGHT = "normal";
const BUTTON_FONT_COLOR = "#007ff9";
const BUTTON_FONT_SIZE = 20;
const HIGHLIGHT_COLOR = "#ebebeb";
const BACKGROUND_COLOR_LIGHT = "#fff";
const BACKGROUND_COLOR_DARK = "#0E0E0E";

const styles = StyleSheet.create({
    contentContainer: {
        justifyContent: "flex-end",
        margin: 10
    },
    datepickerContainer: {
        borderRadius: BORDER_RADIUS,
        marginBottom: 8,
        overflow: "hidden"
    },
    titleContainer: {
        borderBottomColor: BORDER_COLOR,
        borderBottomWidth: StyleSheet.hairlineWidth,
        padding: 14,
        backgroundColor: "transparent"
    },
    title: {
        textAlign: "center",
        color: TITLE_COLOR,
        fontSize: TITLE_FONT_SIZE
    },
    confirmButton: {
        borderColor: BORDER_COLOR,
        borderTopWidth: StyleSheet.hairlineWidth,
        backgroundColor: "transparent",
        height: 57,
        justifyContent: "center"
    },
    confirmText: {
        textAlign: "center",
        color: BUTTON_FONT_COLOR,
        fontSize: BUTTON_FONT_SIZE,
        fontWeight: BUTTON_FONT_WEIGHT,
        backgroundColor: "transparent"
    },
    cancelButton: {
        borderRadius: BORDER_RADIUS,
        height: 57,
        marginBottom: isIphoneX() ? 20 : 0,
        justifyContent: "center"
    },
    cancelText: {
        padding: 10,
        textAlign: "center",
        color: BUTTON_FONT_COLOR,
        fontSize: BUTTON_FONT_SIZE,
        fontWeight: "600",
        backgroundColor: "transparent"
    }
});