import React from 'react';
import {
    Image,
    Animated,
    Text,
    SafeAreaView,
    View,
    LayoutAnimation
} from 'react-native';
import Colors from '../constants/Colors';
export default class LeftColunm extends React.Component {
    state = {
        image: undefined
    }

    componentDidMount() {
        const { percent } = this.props;
        this.getImage(percent)
    }

    componentDidUpdate(prevProps) {
        const { percent } = this.props;
        if (prevProps.isFocused !== this.props.isFocused) {
            this.startAnimation()

        }
        if (prevProps.percent !== percent) {


            this.setState({ first: true })
        }
    }

    startAnimation = () => {
        LayoutAnimation.configureNext({ duration: 700, create: { type: 'linear', property: 'opacity' }, delete: { type: 'spring', property: 'opacity' } })
    }

    render() {
        const { status } = this.props
        const { image } = this.state


        return (
            <SafeAreaView
                style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center' }}>
                <View
                    style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', paddingTop: 30 }}
                >
                    <View>
                        <View>
                            <Text style={{ fontSize: 36, fontWeight: '800', color: Colors.textColor }}>Today</Text>
                        </View>
                        <Image
                            resizeMode='contain'
                            style={{ width: 250, height: 200 }}
                            source={require('../assets/images/cloud.png')}
                        />
                        <View style={{ position: 'absolute', justifyContent: 'flex-start', alignItems: 'flex-start', width: 250, paddingTop: 100, bottom: 0, top: 0, left: 0, right: 0 }}>
                            <Text style={{ fontSize: 36, fontWeight: '800', color: Colors.otherText, left: 40, top: 10 }}>{status}</Text>
                            {/* <Text style={{ fontSize: 12, fontWeight: '500', color: Colors.links, left: 40, top: 10 }}>Check the recommendations</Text> */}
                        </View>

                    </View>
                    <Animated.Image
                        style={{ width: 150, height: 150 }}
                        source={image}
                    />
                </View>
            </SafeAreaView>
        );
    }

    getImage = (value) => {
        const { setStatus } = this.props;
        // console.log('value', value);

        let persent = value * 100
        switch (true) {
            case persent >= 0 && persent <= 20:
                setStatus("Bad")
                this.setState({ image: require('../assets/images/bad.png') })
                break;
            case persent > 20 && persent <= 40:
                setStatus("So sad")
                this.setState({ image: require('../assets/images/sad.png') })
                break;
            case persent > 40 && persent <= 60:
                setStatus("Come on")
                this.setState({ image: require('../assets/images/ok.png') })
                break;
            case persent > 60 && persent <= 95:
                setStatus("Very nice")
                this.setState({ image: require('../assets/images/nice.png') })
                break;
            case persent > 95:
                // console.log('persent', persent);

                setStatus("Amazing")
                this.setState({ image: require('../assets/images/good.png') })
                break;
            default:
                this.setState({ image: require('../assets/images/good.png') })
                break;
        }
    }
}