import React, { Component } from "react";
import { LinearGradient } from 'expo-linear-gradient';

export default class GradientHelper extends Component {
    render() {
        const { style, colors } = this.props;
        return (
            <LinearGradient
                colors={colors}
                // start={{
                //     x: startX,
                //     y: startY
                // }}
                // end={{
                //     x: endX,
                //     y: endY
                // }}
                style={{ ...style }}
            />
        );
    }
}