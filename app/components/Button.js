import React from 'react';
import { TouchableOpacity, Image } from "react-native";
import constants from '../constants/Layout';
const { width, height } = constants.window;
class Button extends React.Component {

    render() {
        const { onPress = () => console.log("press", idToPress), idToPress = "test" } = this.props
        return (
            <TouchableOpacity
                style={{ height: 60 }}
                onPress={() => onPress()}>
                <Image
                    style={{ resizeMode: 'contain', width: width * 0.5, height: 60 }}
                    source={require('../assets/images/button.png')}
                />
            </TouchableOpacity>
        );
    }
}

export default Button;