import React from 'react';
import {
    Image,
    Easing,
    Animated,
    StyleSheet,
    Text,
    LayoutAnimation,
    View,
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import constants from '../constants/Layout';
import Colors from '../constants/Colors';
import GradientHelper from "./GradientHelper";

// const AnimatedGradientHelper = Animated.createAnimatedComponent(GradientHelper);
const { width, height } = constants.window
export default class ColorBar extends React.Component {
    constructor(props) {
        super(props);
        this.heightBar = new Animated.Value(props.percent * height);
        this.heightIntepolate = this.heightBar.interpolate({
            inputRange: [0, 1],
            outputRange: [0, props.percent * height]
        });
        this.state = {
            heightBarState: 0,
            heightIsGet: false
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.startAnimation();
        }, 300);

    }

    componentDidUpdate(prevProps, prevState) {
        const { percent } = this.props;
        const { heightBarState } = this.state;
        if (prevProps.percent !== percent) {
            this.heightBar.setValue(percent * heightBarState)
            LayoutAnimation.configureNext({ duration: 700, create: { type: 'linear', property: 'opacity' }, delete: { type: 'spring', property: 'opacity' } })
        }
    }

    startAnimation = () => {
        const { heightBarState } = this.state;
        Animated.timing(
            this.heightBar,
            {
                toValue: this.props.percent * heightBarState,
                duration: 500,
                easing: Easing.ease
            },

        ).start();
        LayoutAnimation.configureNext({ duration: 700, create: { type: 'linear', property: 'opacity' }, delete: { type: 'spring', property: 'opacity' } })
    }

    onLayout = (value) => {
        const { heightIsGet } = this.state
        if (!heightIsGet) {
            console.log('====================================')
            console.log(value.nativeEvent.layout.height)
            console.log('====================================')
            this.setState({ heightBarState: value.nativeEvent.layout.height, heightIsGet: true })
        }
    }

    render() {
        const { heightBarState } = this.state
        const { percent } = this.props
        const scale = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        return (
            <View
                style={[styles.chartHappinessViewWrapper]}
                onLayout={this.onLayout}
            >
                <View style={{ flex: 0.2, marginRight: 5 }}>
                    {scale.map(
                        (item, index) => <ScaleItem key={item} isLast={scale.length - 1 === index} />
                    )}

                </View>
                <View style={styles.chartHappinessView}>
                    <LinearGradient

                        colors={Colors.ColorContainerBar}
                        style={[styles.chartHappinessLinearGradient, { height: heightBarState }]}>
                        <Animated.View
                            style={[styles.chartHappinessLinearGradient,
                            {
                                height: this.heightBar,
                                overflow: 'hidden',
                                borderTopLeftRadius: 0,
                                borderTopRightRadius: 0,
                                borderBottomLeftRadius: 0,
                                borderBottomRightRadius: 0
                            }]}
                        >
                            <LinearGradient
                                colors={Colors.ColorBar}
                                style={[styles.chartHappinessLinearGradient, { height: heightBarState }]}>

                            </LinearGradient>
                        </Animated.View>
                    </LinearGradient>

                </View>
            </View >
        );
    }
}

class ScaleItem extends React.Component {
    render() {
        const { isLast } = this.props
        return (
            <View style={{ flex: 1, borderBottomColor: isLast ? null : Colors.border, borderBottomWidth: isLast ? null : 1 }}>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    chartHappinessViewWrapper: {
        flexDirection: 'row',
        flex: 1,
        // maxWidth: 60,
        justifyContent: 'flex-end'
    },
    chartHappinessView: {
        borderTopLeftRadius: 23,
        borderTopRightRadius: 23,
        borderBottomLeftRadius: 23,
        overflow: 'hidden',
        maxWidth: 60,
        flex: 0.8
    },
    chartHappinessLinearGradient: {
        borderTopLeftRadius: 23,
        borderTopRightRadius: 23,
        borderBottomLeftRadius: 23,
        borderBottomRightRadius: 0,
        flex: 1,
        // height: '100%',
        // width: '100%',
        minWidth: 60,
        position: 'absolute',
        right: 0,
        bottom: 0
    },
    happinessTitleText: {
        width: '100%',
        position: 'absolute',
        left: 0,
        bottom: 0,
        // fontFamily: 'roboto-300',
        fontSize: 16,
        lineHeight: 19,
        textTransform: 'uppercase',
        color: '#266BDA',
        transform: [{ rotate: '-90deg' }, { translateX: 50 }, { translateY: -35 }],
    },
    chartHealthView: {
        flex: 0.78, // 0.5 min + 0.56 / 2
        borderTopLeftRadius: 23,
        borderTopRightRadius: 23,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
        overflow: 'hidden',
    },
    chartRomanceView: {
        flex: 1,
        height: '100%',
        borderTopLeftRadius: 23,
        borderTopRightRadius: 23,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
        overflow: 'hidden',
    },
    chartCareerView: {
        flex: 0.64, // 0.5 min + 0.28 / 2
        borderTopLeftRadius: 23,
        borderTopRightRadius: 23,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 23,
        overflow: 'hidden',
    },
    gradientCircleSm: {
        width: 4,
        height: 4,
        position: 'absolute',
        left: 15,
        top: 10,
        borderRadius: 2,
    },
    gradientCircleBig: {
        width: 9,
        height: 9,
        position: 'absolute',
        left: 8,
        top: 15,
        borderRadius: 4.5,
    },
    happinessCircle: {
        backgroundColor: '#BDEEFF',
    }
});