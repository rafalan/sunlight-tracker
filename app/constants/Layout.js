import { Dimensions, Platform } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export const isIphoneX = () => {
  const { height, width } = Dimensions.get("window");

  return (
    Platform.OS === "ios" &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    (height === 812 || width === 812 || (height === 896 || width === 896))
  );
};

export function getDaysInMonth(month, year) {
  let date = new Date(Date.UTC(year, month, 1));
  let days = [];

  while (date.getMonth() === month) {
    days.push({ date: new Date(date), selected: false, id: new Date(date) });
    date.setDate(date.getDate() + 1);
  }
  return days;
}

export default {
  window: {
    width,
    height,
  },
  isSmallDevice: width < 375,
};
