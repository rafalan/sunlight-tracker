const tintColor = '#f6c965';

export default {
  tintColor,
  tabIconDefault: '#fff',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
  backgroundColorGradient: ['#313a77', '#272844'],
  backgroundColorTabBar: '#4f8ff7',
  ColorBar: ['#e44ca2', '#e98981', '#f5c763', '#75d7d7', '#79e4c9'].reverse(),
  ColorContainerBar: ['#7f7f87', '#7f7f87'],
  textColor: '#fff',
  otherText: '#000',
  links: 'blue',
  border: '#fff',
  selectedChipColor: '#0097D8',
  chipColor: '#DCE4ED',
  chipTextSelected: '#fff',
  chipText: '#485860',
  backgroundColorCharts: '#7bd0fa'
};
