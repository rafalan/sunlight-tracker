"""Initial schema

Revision ID: 421d5fc7c592
Revises: 
Create Date: 2019-11-17 13:19:19.550440

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "421d5fc7c592"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "user",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("first_name", sa.String(255)),
        sa.Column("last_name", sa.String(255)),
        sa.Column("email", sa.String(255)),
        sa.Column("dob", sa.Date()),
        sa.Column("gender", sa.String(1)),
        sa.Column("skin_factor", sa.Integer()),
        sa.Column("prev_track_ts", sa.Integer()),
        sa.Column("sunlight_amount", sa.Integer()),
        sa.PrimaryKeyConstraint("id"),
    )

    op.create_table(
        "track",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("user_id", sa.Integer()),
        sa.Column("timestamp", sa.DateTime()),
        sa.Column("status", sa.String(3)),  # in/out
        sa.Column("lat", sa.Float()),
        sa.Column("lon", sa.Float()),
        sa.PrimaryKeyConstraint("id"),
        sa.ForeignKeyConstraint(["user_id"], ["user.id"], name="fk_track_user", ondelete="CASCADE"),
    )

    op.create_table(
        "weather",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("date", sa.Date),
        sa.Column("hour", sa.Integer()),  # We update data every hour
        sa.Column("lat", sa.Float()),  # Rounded values for lat & lon
        sa.Column("lon", sa.Float()),
        sa.Column("sunlight_intensity", sa.Integer()),
        sa.PrimaryKeyConstraint("id"),
        sa.Index("ix_location_sunlight", "date", "hour", "lat", "lon"),
    )

    op.execute(
        'INSERT INTO user SET id = 123, first_name = "John", last_name = "Doe", email = "john@doe.com", dob = "1990-01-01", gender = "m", skin_factor = 1, sunlight_amount = 0'
    )  # Demo user


def downgrade():
    op.drop_table("weather")
    op.drop_table("track")
    op.drop_table("user")
