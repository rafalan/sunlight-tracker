#!/bin/bash

echo "Checking database connection at host: ${SUNLIGHT_DB_HOST:-db}"
while ! mysqladmin ping -h "${SUNLIGHT_DB_HOST:-db}" --silent; do
    echo "Waiting for database connection at host: ${SUNLIGHT_DB_HOST:-db}"
    sleep 1
done

case $1 in
*)
    alembic upgrade head

    if [ "$SUNLIGHT_MODE" = "prod" ];
    then
        echo "Starting WEB-backend service using Gunicorn server"
        exec gunicorn --bind 0.0.0.0:8000 --workers 6 --timeout 15 --log-level info --access-logfile - --access-logformat '%({X-Forwarded-For}i)s - %(t)s - "%(r)s" %(s)s %(b)s %(D)s "%(f)s"' ${@:2} run:app
    else
        echo "Starting WEB-backend service using Flask server"
        exec python run.py ${@:2}
    fi
    ;;
esac
