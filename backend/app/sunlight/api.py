import datetime
import logging
from flask_restplus import Api, Resource, fields
from flask import request
from .models import Track, User
from .database import db_session
from .solcast import get_intensity

logger = logging.getLogger(__name__)
api = Api(title="Sunlight Tracker API")

resource_fields = api.model(
    "Resource", {"timestamp": fields.Integer, "status": fields.String, "lat": fields.Float, "lon": fields.Float}
)


@api.route("/health")
class HealthResource(Resource):
    def get(self):
        return {"status": "running"}


@api.route("/api/tracker/<int:user_id>")
class TrackerResource(Resource):
    @api.expect(resource_fields)
    def post(self, user_id):
        payload = request.json

        if payload.get("status") == "out":
            user = User.query.filter_by(id=user_id).first()

            if user.prev_track_ts:
                period = payload.get("timestamp") - user.prev_track_ts

                logger.warn(period)

                intensity = get_intensity(payload.get("lat"), payload.get("lon"))
                user.sunlight_amount = user.sunlight_amount + period * intensity * user.skin_factor

            user.prev_track_ts = payload.get("timestamp")
            db_session.add(user)

        track = Track(
            user_id=user_id,
            timestamp=datetime.datetime.fromtimestamp(payload.get("timestamp")).strftime("%Y-%m-%d %H:%M:%S"),
            status=payload.get("status"),
            lat=payload.get("lat"),
            lon=payload.get("lon"),
        )

        db_session.add(track)
        db_session.commit()

        return {"status": "track saved"}


@api.route("/api/user-details/<int:user_id>")
class UserResource(Resource):
    def get(self, user_id):
        user = User.query.filter_by(id=user_id).first()

        return {
            "first_name": user.first_name,
            "last_name": user.last_name,
            "email": user.email,
            "dob": str(user.dob),
            "gender": user.gender,
        }


@api.route("/api/user-amount/<int:user_id>")
class UserResource(Resource):
    def get(self, user_id):
        user = User.query.filter_by(id=user_id).first()

        return {
            "amount": user.sunlight_amount,
            "percent": (user.sunlight_amount * 100) / 720000
        }