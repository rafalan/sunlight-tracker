from sqlalchemy import Column, Date, DateTime, Float, Integer, String
from .database import Base


class User(Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True)
    first_name = Column(String(255))
    last_name = Column(String(255))
    email = Column(String(255))
    dob = Column(Date)
    gender = Column(String(1))
    skin_factor = Column(Integer)
    prev_track_ts = Column(Integer)
    sunlight_amount = Column(Integer)


class Track(Base):
    __tablename__ = "track"
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    timestamp = Column(DateTime)
    status = Column(String(3))
    lat = Column(Float)
    lon = Column(Float)


class Weather(Base):
    __tablename__ = "weather"
    id = Column(Integer, primary_key=True)
    date = Column(Date)
    hour = Column(Integer)
    lat = Column(Float)
    lon = Column(Float)
    sunlight_intensity = Column(Integer)
