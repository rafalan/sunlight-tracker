import os
import logging
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from functools import lru_cache

logger = logging.getLogger(__name__)
api_key = os.getenv("SOLCAST_API_KEY")


def multi_retry_request(url, method="get", params=None, json=None, data=None):
    session = requests.Session()

    retries = Retry(total=5, backoff_factor=2, status_forcelist=[420, 500, 502, 503, 504])

    session.mount("http://", HTTPAdapter(max_retries=retries))
    session.mount("https://", HTTPAdapter(max_retries=retries))

    logger.debug("Request (%s): %s" % (method, url))

    response = getattr(session, method)(url, params=params, json=json, data=data)

    response.raise_for_status()

    if "application/json" in response.headers.get("content-type"):
        return response.json()
    else:
        return response.content


@lru_cache(65536)
def get_intensity(lat, lon):
    if os.getenv("SUNLIGHT_MODE") == "dev":
        return 200

    url = "https://api.solcast.com.au/world_radiation/forecasts?hours=1&format=json"
    url += f"latitude={lat}&longitude={lon}&api_key={api_key}"

    response = multi_retry_request(url)

    intensity = response["forecasts"][0]["ghi"]
    logger.debug(f"intensity={intensity}")

    return intensity
