# import htmlmin
import logging
import os
# from functools import lru_cache
# import re
# import sunlight.web.assets as assets
# from datetime import datetime
from flask import Flask, request
# from flask_session import Session
# from raven.contrib.flask import Sentry
# from sunlight.web.redis import redis_sessions
# from sunlight.web.views.public import public
# from sunlight.web.database import db_session
# from sunlight.web.views.admin import admin
from .models import User, Track, Weather

logger = logging.getLogger(__name__)
# sess = Session()


def create_app():
    app = Flask(__name__)

    # if os.getenv('SUNLIGHT_MODE') == 'prod':
    #     if os.getenv('SUNLIGHT_RAVEN_DSN'):
    #         sentry = Sentry(dsn=os.getenv('SUNLIGHT_RAVEN_DSN'))
    #         sentry.init_app(app)

    # app.config['SESSION_TYPE'] = 'redis'
    # app.config['SESSION_REDIS'] = redis_sessions
    # app.config['SESSION_COOKIE_NAME'] = 'sunlight_session'
    # app.config['SESSION_COOKIE_SECURE'] = True
    # app.config['PERMANENT_SESSION_LIFETIME'] = 86400

    # app.register_blueprint(public)
    # # app.register_blueprint(admin, url_prefix='/admin')

    # if os.getenv('SUNLIGHT_MODE') == 'dev':
    #     app.jinja_env.auto_reload = True
    # else:
    #     app.jinja_env.auto_reload = False

    # sess.init_app(app)

    # @app.teardown_appcontext
    # def shutdown_session(exception=None):
    #     db_session.remove()

    from .api import api
    api.init_app(app)

    return app
